### microservices class proposal [it-ntl]

Anthony Sayre - 12/19/17

asayre@sofree.us

**note** - a rough outline on how I'd like a micro-services class to go.

this is mostly a reaction to a micro-services workshop I attended that assumed everyone already understood what micro-services are and immediately began showing us their cool products. I did not understand micro-services well and after asking several folks in the class, neither did many others. These were mostly experienced techies so if they didn't understand it well, its a good bet others probably don't either and there may be some interest in a gentle hands-on introduction to micro-services (aghi2microservices).

**what I'd like from anyone who has experience in this area and might want to help**

* **advice** on overall idea and approach - Am I wrong in assuming there would be broad interest in this? Would there be at least some narrow interest that would make it worthwhile? What about my below class outline would you change? I am not approaching this is an SME on micro-services but a learner myself so I definitely want input form others.

* **content** - anywhere someone thinks they could fill in some content I'd love to hear from you. There would be some coding to demo and other stuff too.

You can be a teacher/presenter of one of the below sections or contribute written content, or both. In any case you will be credited publicly, in the class and in our meetup pages (meetup.com/it-ntl and meetup.com/sofreeus) and anywhere else we can think of.
___

**microservices class outline**

1-day class - 4 hours - 4 topics, 1 topic per hour

***hour 1*** - what are microservices and why do they matter?
  * lecture (15 minutes max)

  * demonstrate a microservice in action (15 minutes max) - suggestion: I believe **ibotta** uses micro-services for their e-commerce app? could we demo the use of the app and talk about how it works during demo?

  * class then does the same demo (30 minutes max)

***hour 2*** - how are micro-services built? UI example

**note** - I have jboss/openshift from redhat on a laptop and can put together a lecture and demo of this as a UI based platform for micro-services creation. If anyone has another UI based platform that might be better I am open to it.

* lecture (15 minutes max)

* demonstrate creation of a microservice using UI/IDE (15 minutes max)

* class then does the same demo (30 minutes max)

***hour 3*** - how are micro-services built? coding example - python, java, or...?

**note** - this is where we pull back the curtain on micro-services and demonstrate that you don't need a glossy IDE specifically for micro-services. anyone with some coding skills can do it with bash, python, java, etc...

additionally I have local access to **ASG's kubernetes** cluster and other hardware in our ilab where we can spin up containers to host services

* lecture (15 minutes max)

* demonstrate creation of a microservice using python, java, bash, other... (15 minutes max)

* class then does the same demo (30 minutes max)


***hour 4*** - not sure! maybe this is only a 3-hour class or (more likely) one of the other sections goes long and this hour is for overflow
